#!/bin/sh

set -e

VERSION=$2
SOURCE=$(dpkg-parsechangelog | sed -ne 's,Source: \(.*\),\1,p')

DIR=ibatis
TAR=../${SOURCE}_${VERSION}.orig.tar.gz

mkdir $DIR
unzip -d $DIR $3
(cd $DIR/src && unzip ibatis-src.zip && rm ibatis-src.zip)
tar -c -z -f $TAR --exclude '*.jar' --exclude '*/doc/*' $DIR
rm -rf $DIR $3

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

